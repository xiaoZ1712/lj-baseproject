package com.comleader.baseproject.web.controller;

import java.io.File;

/**
 * @Author: Daisen.Z
 * @Date: 2022/11/17 11:33
 * @Version: 1.0
 * @Description:
 */
public class Test {

    private static String preName = "htm";
    private static  String suffixName = "html";

    public static void main(String[] args) {
        File file  = new File("D:\\信大网御\\我的项目\\业务成果库2.0\\模板制作\\27513\\templets");
        renameFileName(file);
    }


    public static void renameFileName(File file){
        for (File f :file.listFiles()){
            if(f.isDirectory()){
                renameFileName(f);
            }else {
                String name = f.getName();
                String substring1 = name.substring(name.lastIndexOf(".")+1);
                System.out.println(f.getName().substring(f.getName().lastIndexOf(".")+1));
                if (preName.equals(f.getName().substring(f.getName().lastIndexOf(".")+1))) {
                    String absolutePath = f.getAbsolutePath();
                    String substring = absolutePath.substring(0, absolutePath.lastIndexOf(".") + 1);
                    String finalName = substring + suffixName;
                    f.renameTo(new File(finalName));
                }
            }
        }

    }
}
