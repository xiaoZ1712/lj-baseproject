package com.comleader.baseproject.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Daisen.Z
 * @Date: 2021/7/13 14:01
 * @Version: 1.0
 * @Description:
 */
@ApiModel(value = "user对象", description = "用户对象user")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户名", name = "username", example = "xingguo")
    private String username;
    @ApiModelProperty(value = "状态", name = "state", required = true)
    private Integer state;
    private String password;
    private String nickName;
    private Integer isDeleted;
    @ApiModelProperty(value = "id数组", hidden = true)
    private String[] ids;
    private List<String> idList; //省略get/set
}