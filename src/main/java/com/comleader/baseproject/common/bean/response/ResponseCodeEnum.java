package com.comleader.baseproject.common.bean.response;

import lombok.ToString;

/**
 * @Author: mrt.
 * @Description:
 * @Date:Created in 2018/1/24 18:33.
 * @Modified By:
 */

@ToString
public enum ResponseCodeEnum implements CommonCode {

    // 操作类响应码
    SUCCESS(true,10000,"操作成功！"),

    //错误提示信息
    FAIL(false,11111,"操作失败！"),
    INVALIDPARAM(false,11112 ,"非法参数" ),
    UNAUTHENTICATED(false,11113,"此操作需要登陆系统！"),
    UNAUTHORISE(false,11114,"权限不足，无权操作！"),
    NULL_RESULT(false,11115,"查询结果为空!"),

    // 系统类响应码 12000
    SERVER_ERROR(false,12000,"抱歉，系统繁忙，请稍后重试！");


    //操作是否成功
    boolean success;
    //操作代码
    int code;
    //提示信息
    String message;
    private ResponseCodeEnum(boolean success, int code, String message){
        this.success = success;
        this.code = code;
        this.message = message;
    }

    @Override
    public boolean success() {
        return success;
    }
    @Override
    public int code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

}
