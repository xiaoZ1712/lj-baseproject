package com.comleader.baseproject.common.bean.response;

import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author: mrt.
 * @Description:
 * @Date:Created in 2018/1/24 18:33.
 * @Modified By:
 */
@ToString
@NoArgsConstructor
public class ResponseResult<T> implements Response {

    //操作是否成功
    private boolean success = SUCCESS;

    //操作代码
    private int code = SUCCESS_CODE;

    //提示信息
    private String message;

    // 数据信息
    private T data;

    public ResponseResult(CommonCode resultCode){
        this.success = resultCode.success();
        this.code = resultCode.code();
        this.message = resultCode.message();
    }

    public static ResponseResult SUCCESS(){
        ResponseResult responseResult = new ResponseResult(ResponseCodeEnum.SUCCESS);
        return responseResult;
    }


    public static <T> ResponseResult<T> SUCCESS(T obj){
        ResponseResult<T> responseResult = new ResponseResult(ResponseCodeEnum.SUCCESS);
        responseResult.data = obj;
        return responseResult;
    }



    public static ResponseResult FAIL(){
        return new ResponseResult(ResponseCodeEnum.FAIL);
    }


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
