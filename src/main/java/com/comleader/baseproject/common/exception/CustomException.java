package com.comleader.baseproject.common.exception;

import com.comleader.baseproject.common.bean.response.CommonCode;

/**
 * @author 张志航     
 * @version V1.0.0
 * @projectName xc-framework-parent
 * @title     CustomException   
 * @package    com.xuecheng.framework.exception  
 * @date   2019/12/16 11:22  
 * @explain
 */
public class CustomException extends RuntimeException {

    private CommonCode responseCode;

    public CustomException(CommonCode responseCode) {
        //异常信息为错误代码+异常信息
        super("错误代码："+responseCode.code()+"错误信息："+responseCode.message());
        this.responseCode = responseCode;
    }

    public CommonCode getResultCode() {
        return responseCode;
    }
}
