package com.comleader.baseproject.common.config;

import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;


/**
 *  * @Description: 启动后访问： http://ip:端口/模块名/doc.html
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {

    @Bean
    public Docket createRestApi(Environment environment) {
        // 统一设置返回描述
        List<ResponseMessage> responseMessages = new ArrayList<>();
        responseMessages.add(new ResponseMessageBuilder().code(200).message("请求成功").build());
        responseMessages.add(new ResponseMessageBuilder().code(400).message("参数错误").build());
        responseMessages.add(new ResponseMessageBuilder().code(401).message("没有权限").build());
        responseMessages.add(new ResponseMessageBuilder().code(403).message("禁止访问").build());
        responseMessages.add(new ResponseMessageBuilder().code(404).message("请求路径错误").build());
        responseMessages.add(new ResponseMessageBuilder().code(410).message("没有登录").build());
        responseMessages.add(new ResponseMessageBuilder().code(411).message("发生异常").build());
        responseMessages.add(new ResponseMessageBuilder().code(412).message("内容不存在").build());
        responseMessages.add(new ResponseMessageBuilder().code(414).message("token过期").build());
        responseMessages.add(new ResponseMessageBuilder().code(415).message("用户不存在").build());
        responseMessages.add(new ResponseMessageBuilder().code(416).message("密码错误").build());
        responseMessages.add(new ResponseMessageBuilder().code(418).message("验证码错误").build());
        responseMessages.add(new ResponseMessageBuilder().code(419).message("缓存失败").build());
        responseMessages.add(new ResponseMessageBuilder().code(420).message("不支持或已经废弃").build());
        responseMessages.add(new ResponseMessageBuilder().code(445).message("太频繁的调用").build());
        responseMessages.add(new ResponseMessageBuilder().code(499).message("未知错误").build());
        responseMessages.add(new ResponseMessageBuilder().code(500).message("系统错误").build());
        responseMessages.add(new ResponseMessageBuilder().code(501).message("空指针错误").build());

        // 设置swagger-ui是否显示，获取spring.profiles.active的值，只有是dev和test时显示swagger-ui
        // 设置要显示的swagger 环境
        Profiles p = Profiles.of("dev","test");
        // 通过environment.acceptsProfiles 判断是否处在自己设定的环境中
        boolean b = environment.acceptsProfiles(p);

        return new Docket(DocumentationType.SWAGGER_2)
                // 禁用默认返回描述
                .useDefaultResponseMessages(false)
                .enable(b) // 只有测试和开发环境显示
                // 设置基本信息
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))//这是注意的代码
                .paths(PathSelectors.any())
                .build()
                // 启用新的返回描述
                .globalResponseMessage(RequestMethod.GET, responseMessages)
                .globalResponseMessage(RequestMethod.POST, responseMessages)
                .globalResponseMessage(RequestMethod.PATCH, responseMessages)
                .globalResponseMessage(RequestMethod.PUT, responseMessages)
                .globalResponseMessage(RequestMethod.DELETE, responseMessages);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("接口文档")
                .description("对外接口")
                .contact(new Contact("高凌信息", "", "zhangzhihang@comleader.com.cn"))
                .version("1.0")
                .termsOfServiceUrl("http://www.comleader.com")
                .build();
    }
}