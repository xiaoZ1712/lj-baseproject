package com.comleader.baseproject.common.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 张志航
 * @description
 * @date 2020-11-25 14:38
 */
@Data
@Component
@PropertySource(value = {"classpath:config/config.properties"}, encoding = "UTF-8")
public class ConfigProperties {

    @Value("${base.test.filePath}")
    private String filePathTmp;
    public static String filePath;

    @PostConstruct
    public void init(){
        // 在类初始化完成后将filePathTmp赋值给静态变量filePath，以供外部可以使用ConfigProperties.filePath的方式调用
        filePath = filePathTmp;
    }

}
