package com.comleader.baseproject.common.util;

import java.text.DecimalFormat;

/**
 * @author 张志航
 * @description  用于转换数据格式的工具类
 * @date 2020-11-25 14:20
 * method:countFileSize: 数据大小格式转换
 * toDuration： 间隔时常毫秒转换为时长
 */
public class TypeFormatUtil {

    /**
     * @param dataSize
     * @description: 数据大小格式转换
     * @return: int
     * @author: zhanghang
     * @date: 2020/4/10
     **/
    public static String countFileSize(double dataSize) {
        // 平均每个图片20K
        double size = dataSize;
        if (size < 1024) {
            return new DecimalFormat("#.##").format(size) + "K";
        } else if (size < 1024 * 1024) {
            size = size / 1024.0;
            return new DecimalFormat("#.##").format(size) + "M";
        } else if (size < 1024 * 1024 * 1024) {
            size = size / 1024.0 / 1024.0;
            return new DecimalFormat("#.##").format(size) + "G";
        } else {
            size = size / 1024.0 / 1024.0 / 1024.0;
            return new DecimalFormat("#.##").format(size) + "T";
        }
    }

    /**
     * @param time
     * @return
     * @description 间隔时间毫秒转换为时长
     * @author zhanghang
     * @date 2020-09-25 12:59:27
     **/
    public static String toDuration(long time) {
        int day = (int) (time / (1000 * 60 * 60 * 24));
        int hours = (int) ((time % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        int minutes = (int) ((time % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) ((time % (1000 * 60)) / 1000);
        StringBuilder duraTime = new StringBuilder();
        if (day > 0) {
            duraTime.append(day + "天");
        }
        if (hours > 0) {
            duraTime.append(hours + "时");
        }
        if (minutes > 0) {
            duraTime.append(minutes + "分");
        }
        duraTime.append(seconds + "秒");
        return duraTime.toString();
    }

}
