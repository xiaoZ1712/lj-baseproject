package com.comleader.baseproject.common.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author 张志航
 * @description  用于下载与导出表格的工具类
 * @date 2020-11-25 14:21
 */
@Component
@PropertySource(value = {"classpath:config/config.properties"}, encoding = "UTF-8")
public class DownFileUtil {

    @Value("${base.test.filePath}")
    private String filePath;

    private static String FILEPTH;

    //当容器实例化当前受管Bean时@PostConstruct注解的方法会被自动触发，借此来实现静态变量初始化
    @PostConstruct
    public void init() {
        this.FILEPTH = filePath;
    }




}
